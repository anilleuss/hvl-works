package com.anilerdogan.company;

import java.util.Date;

import com.anilerdogan.model.Employee;
import com.anilerdogan.service.EmployeeService;
import com.anilerdogan.service.impl.EmployeeServiceImpl;

public class Havelsan {

	public static void main(String[] args) {
		
		EmployeeService empService = new EmployeeServiceImpl();
		empService.createEmployee(3617, "Anil","Erdogan" , "Erkek", new Date("22/10/1984"));
		empService.readEmployee(3617);
		empService.updateEmployee(new Employee(4000, "Ahmet", "Yilmaz", "Erkek", new Date("01/01/1984")));
		empService.deleteEmployee(3617);
		
		
	}
}
