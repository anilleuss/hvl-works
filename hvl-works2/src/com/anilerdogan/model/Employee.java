package com.anilerdogan.model;

import java.util.Date;

public class Employee {

	private Integer empId;
	private String empName;
	private String empLastName;
	private String gender;
	private Date birthDate;

	public Employee(Integer empId, String empName, String empLastName, String gender, Date birthDate) {
		this.empId = empId;
		this.empName = empName;
		this.empLastName = empLastName;
		this.gender = gender;
		this.birthDate = birthDate;
	}
	
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empLastName=" + empLastName + ", gender="
				+ gender + ", birthDate=" + birthDate + "]";
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
