package com.anilerdogan.service;

import java.util.Date;

import com.anilerdogan.model.Employee;

public interface EmployeeService {

	public Employee createEmployee(Integer empId,String name,String surname,String gender,Date birthDate);

	public Integer readEmployee(Integer empId);

	public Employee updateEmployee(Employee employee);

	public Integer deleteEmployee(Integer empId);

}
