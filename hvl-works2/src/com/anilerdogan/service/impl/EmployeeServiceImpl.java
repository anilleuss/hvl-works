package com.anilerdogan.service.impl;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.anilerdogan.model.Employee;
import com.anilerdogan.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService{

	private static Logger logger =Logger.getLogger("EmployeeRecordService");
	
	@Override
	public Employee createEmployee(Integer empId, String name, String surname, String gender, Date birthDate) {
		logger.log(Level.INFO, "Creating Employee in Record!");
		Employee employee = new Employee(empId,name,surname,gender,birthDate);
		System.out.println("----Employee Information----");
		System.out.println(employee);
		return employee;
	}

	@Override
	public Integer readEmployee(Integer empId) {
		logger.log(Level.INFO,"Reading Employee from Record!");
		System.out.println("Updated Employee ID:"+empId);
		return empId;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		logger.log(Level.INFO,"Updating Employee in Record!");
		System.out.println("----Employee Information----");
		System.out.println(employee);
		return employee;
	}

	@Override
	public Integer deleteEmployee(Integer empId) {
		logger.log(Level.INFO,"Deleting Employee from Record!");
		System.out.println("Deleted Employee ID:"+empId);
		return empId;
	}
	
}
