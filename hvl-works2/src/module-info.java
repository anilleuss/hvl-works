module com.anilerdogan {
	exports com.anilerdogan.model;
	exports com.anilerdogan.service;
	exports com.anilerdogan.service.impl;
	requires java.logging;
}